#!/bin/sh
# shellcheck shell=sh # Written to be POSIX compatible

###! Non-standard function used to handle configuration files of compiled target in IMAGE dir prior to merge in the userland
###! SYNOPSIS: function-name [CATEGORY/[PN|PNV|PNVR]...
###! License: Created by Jacob Hrbek identified by GPG identifier assigned to the electronic mail <kreyren@rixotstudio.cz> based on keyserver <https://keys.openpgp.org> under GPLv3 license <https://www.gnu.org/licenses/gpl-3.0.en.html> in 11/10/2020-EU 09:51:21 CEST
###! Bugs-to: https://gitea.com/kreyren/kreyren
imgconf() {
	case "$PALUDIS_DEBUG" in *"func"*) ${SET:-set} -x; esac

	# Krey: Define input
		package="$1"

	# Krey: Provide name of the hook to be used in the logic
		hookName="imgconf"

	${EDEBUG:-edebug} func "Starting hook '$hookName'"

	# Krey: Process variables
		[ -n "$PALUDIS_DIR" ] || PALUDIS_DIR="/etc/paludis/"
		${EDEBUG:-edebug} var "Variable 'PALUDIS_DIR' assigned value '$PALUDIS_DIR'"

		[ -n "$PALUDIS_PACKAGE_CONFIG_DIR" ] || PALUDIS_PACKAGE_CONFIG_DIR="${PALUDIS_DIR:-/etc/paludis}/$hookName"
			PPCD="$PALUDIS_PACKAGE_CONFIG_DIR"
		${EDEBUG:-edebug} var "Variable 'PALUDIS_PACKAGE_CONFIG_DIR' assigned value '$PALUDIS_PACKAGE_CONFIG_DIR'"
		${EDEBUG:-edebug} var "Variable 'PPCD' assigned value '$PPCD'"

		[ -n "$PALUDIS_PACKAGE_CONFIG_FILE" ] || PALUDIS_PACKAGE_CONFIG_FILE="${PALUDIS_DIR:-/etc/paludis}/$hookName"
			PPCF="$PALUDIS_PACKAGE_CONFIG_FILE"
		${EDEBUG:-edebug} var "Variable 'PALUDIS_PACKAGE_CONFIG_FILE' assigned value '$PALUDIS_PACKAGE_CONFIG_FILE'"
		${EDEBUG:-edebug} var "Variable 'PPCF' assigned value '$PPCF'"

	# Krey: Process input
		case "$package" in
			"$CATEGORY/$PN" | "$CATEGORY/$PNV" | "$CATEGORY/$PNVR")
				if [ -d "$PPCD/$package" ]; then
					for pathname in "$PPCD"/"$package"/*; do
						${CP:-cp} -r "$pathname" "$IMAGE" || ${DIE:-die} hookfail "Hook '$hookName' is unable to copy '$pathname' to '$IMAGE'"
						${DIE:-einfo} hooksuccess "Hook '$hookName' finished processing '$package'"
						return 0 # paludis-compat
					done
				elif [ ! -d "$PPCD/$package" ]; then
					${DIE:-einfo} hooksuccess "Hook '$hookName' did not find any configuration files in '$PPCD/$package', skipping.."
					${UNSET:-unset} hookName
					${RETURNL:-return} 0
				else
					${DIE:-die} bug "processing '$PPCD/$CATEGORY/$PN'"
				fi
			;;
			*) ${DIE:-die} syntaxerr "Hook '$hookName' was provided with invalid input: $*"
		esac

	case "$PALUDIS_DEBUG" in *"func"*) ${SET:-set} +x; esac

	${UNSET:-unset} hookName
}
