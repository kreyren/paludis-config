# shellcheck shell=sh # Paludis doesn't expect shebang here, Written to be POSIX compatible

worldFile="/etc/paludis/world"

# Source bashrc
[ ! -d "/etc/paludis/bashrc" ] || . /etc/paludis/bashrc

###! This function expects PCRE RegEx expression from the world file
checkpkg() { ${GREP:-grep} -qP "$1" "$worldFile" ;}

# GIT
##@ +pcre = To get PCRE regex for 'git grep'
##@ +curl = Allows using http/https URI
checkpkg "^dev-scm\/git\$" && ${PRINTF:-printf} '%s\n' \
	"dev-scm/git::arbor curl pcre" || true

checkpkg "^sys-apps\/sydbox\$" && ${PRINTF:-printf} '%s\n' \
	"sys-apps/sydbox::arbor seccomp" || true

# BIND9
##@ caps = Fails to build without it (https://gitlab.exherbo.org/exherbo/arbor/-/issues/45)
checkpkg "^net-dns\/bind\$" && ${PRINTF:-printf} '%s\n' \
	"net-dns/bind::arbor caps" || true

# Dovecot
checkpkg "^net-mail\/dovecot\$" && ${PRINTF:-printf} '%s\n' \
	"net-mail/dovecot::arbor arbon2 tcpd"

# SSH
##@ X509 = To generate X.509 certs
##@ lsns = For DNSSEC support (according to exherbo)
checkpkg "^net-misc\/openssh\$" && ${PRINTF:-printf} '%s\n' \
	"net-misc/openssh::arbor X509 ldns" || true

# Mutt
checkpkg "^mail-client\/mutt\$" && ${PRINTF:-printf} '%s\n' \
	"mail-client/mutt::arbor ncurses sasl gdbm" || true
# Vim
##@ gpm = Adds support for sys-libs/gpm (Console-based mouse driver)
##@ 
checkpkg "^app-editors\/vim\$" && ${PRINTF:-printf} '%s\n' \
	"app-editors/vim::arbor gpm" || true

# Jobs
${PRINTF:-printf} "*/* BUILD_OPTIONS: jobs=%s\\n" "$(${NPROC:-nproc} || ${PRINTF:-printf} 1)"

# CPU flags
AMD64_CPU_FEATURES="$(${CAT:-cat} /proc/cpuinfo  | ${GREP:-grep} -m 1 flags | ${SED:-sed} 's/^flags.*: //g')"
X86_CPU_FEATURES="$(${CAT:-cat} /proc/cpuinfo  | ${GREP:-grep} -m 1 flags | ${SED:-sed} 's/^flags.*: //g')"
